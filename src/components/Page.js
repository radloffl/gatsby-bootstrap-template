import * as React from 'react';
import Navbar from './Navbar';
import Footer from "../components/Footer"

const Page = ({children, title, heading}) => {
    return (<main>
            <title>{title}</title>
            <Navbar/>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1>{heading ? heading : title}</h1>
                    </div>
                </div>
                {children}
            </div>
            <Footer/>
        </main>
    )
}

export default Page