import * as React from "react"
import Page from "../components/Page"

// markup
const IndexPage = () => {
  return (<Page title="Home Page" heading="Gatsby Bootstrap Template">
      <div className="row">
        <div className="col">
          <p>
            Edit <code>src/pages/index.js</code> to see this page
            update in real-time.{" "}
            <span role="img" aria-label="Sunglasses smiley emoji">
              😎
            </span>
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-header">
              Example
            </div>
            <div className="card-body">
              <h5 className="card-title">Card</h5>
              <p className="card-text">This is some text within a card body.</p>
            </div>
            <div className="card-footer">
              Card footer
            </div>
          </div>
          
        </div>
      </div>
    </Page>
  )
}

export default IndexPage
